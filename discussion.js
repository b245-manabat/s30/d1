db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);



// [SECTION] MongoDB Aggregation
  /*
    - to generate and perform operations to create filtered results that helps us analyze the data.
  */

  /* using aggregate method:
    - the $match is used to pass the documents that meet the specified condition/s to the next stageo of aggregation process
      Syntax:
        {$match:{field: value}};
        */

  db.fruits.aggregate([{$match : {onSale: true}}]);

  /*
    - the group is used to group the elements together and field-value the data from the grouped element
      Syntax:
        {$group: _id: "fieldSetGroup"}
  */

  db.fruits.aggregate([
    {$match : {onSale: true, name: "Apple"}},
    {$group : {_id:"$supplier_id", totalFruits : {$sum : "$stock"}}}
    ]);

  // Mini-Activity

  db.fruits.aggregate([
    {$match : {color: "Yellow"}},
    {$group : {_id:"$supplier_id", totalFruits : {$sum : "$stock"}}}
    ]);

  // Field Projection with aggregation
    /*
      - $project can be used when aggregating data to include / exclude from the returned result.
        Syntax:
          {$project: {field: 1/0}};
    */

  db.fruits.aggregate([
    {$match : {onSale: true}},
    {$group : {_id:"$supplier_id", totalFruits : {$sum : "$stock"}}},
    {$project: {_id:0}}
    ]);

  // sorting aggregated results
    /*
      - $sort can be used to change the order of the aggregated result
    */

  db.fruits.aggregate([
    {$match : {onSale: true}},
    {$group : {_id:"$supplier_id", totalFruits : {$sum : "$stock"}}},
    {$sort: {totalFruits:1}}
    ]);

  /*the value in sort
    1 - lowest to highest
    -1 - highest to lowest*/

  // aggregating result based on an array fields
    /*
      the $unwind deconstructs an array field from a collection/field with an array value to output a result
    */

  db.fruits.aggregate([
      {$unwind: "$origin"},
      {$group: {_id: "$origin", fruits: {$sum:1}}}
    ]);

// [SECTION] other aggregate stages
  // $count all yellow fruits

  db.fruits.aggregate([
        {$match: {color: "Yellow"}},
        {$count: " Yellow Fruits"}
      ]);

  // $avg gets the average value of the stock

  db.fruits.aggregate([
        {$match:  {color:"Yellow"}},
        {$group: {_id: "$color", avgYellow: {$avg:"$stock"}}}
    ]);

  // $min && $max

  db.fruits.aggregate([
        {$match:  {color:"Yellow"}},
        {$group: {_id: "$color",  lowestStock: {$min:"$stock"}}}
    ]);

  db.fruits.aggregate([
        {$match:  {color:"Yellow"}},
        {$group: {_id: "$color",  lowestStock: {$max:"$stock"}}}
    ]);